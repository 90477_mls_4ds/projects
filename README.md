# List of projects

Projects for the final examination are related to the following topics: COVID 19, Software Metrics and Log Analysis. **In case you are not able to download the paper please contact professor.**

**Note**: Each project can be developed by one student or by a small group of 2/3 students.  

Identifier|Title|Year|URL|Data|State|Badge Number(BN)[1,3]
-|-|-|-|-|-|-
Project (PR) Identifier| |Year of the publication specified in the URL|Link to the project|Available|Available|University Badge Numbers of students involved in the project
|||||Contact Professor|Not Available|
||||||Done|


COVID 19

Identifier|Title|Year|URL|Data|State|BN1|BN2|BN3 
-|-|-|-|-|-|-|-|-
**PR1**|Association of NO2 levels with SARS-COV-2|2020|[SAtellite-detected tropospheric nitrogen dioxide and spread of SARS-CoV-2 infection in Northern Italy](https://www.sciencedirect.com/science/article/pii/S0048969720337992)|Contact Professor (Shared data)|Done|1900095930|NA|NA
**PR2**|Association of NO2 levels with SARS-COV-2|2021|[Associations between mortality from COVID-19 in two Italian regions and outdoor air pollution as assessed through tropospheric nitrogen dioxide](https://www.sciencedirect.com/science/article/pii/S0048969720368868)|Available|Done|1057715|1032944|1058296
**PR3**|Effect of the COVID-19 lockdown on pollution in CZ|2021|[Effect of the COVID-19 Lockdown on Air Pollution in the Ostrava Region](https://www.mdpi.com/1660-4601/18/16/8265)|Contact Professor (Shared data)|Done|0001057217|0001034982|0001057681
PR4|Sentiment Analysis for COVID-19 Twitter Data|2021|[Sentiment Analysis in Indian Sub-continent During COVID-19 Second Wave using Twitter Data](https://ieee-dataport.org/documents/covid-19-second-wave-tweets-annotations)|Contact Professor (Shared data)|Not Available|1036045|NA|NA
**PR5**|Tweets about coronavirus pandemic events|2020|[Predicting Coronavirus Pandemic in Real-Time Using Machine Learning and Big Data Streaming System](https://www.hindawi.com/journals/complexity/2020/6688912/)|Contact Professor (Shared data)|Done|1024924|1025999|NA
**PR6**|Seasonability of coronavirus disease|2020|[Temperature, Humidity, and Latitude Analysis to Estimate Potential Spread and Seasonability of Coronavirus Disease 2019 (COVID-19)](https://jamanetwork.com/journals/jamanetworkopen/fullarticle/2767010)|Contact Professor (Shared data)|Done|001035754|0001026530|0001028507
**PR7**|Relationship between PM2.5 and PM10 impact on COVID-19|2020|[Assessing the relationship between surface levels of PM2.5 and PM10 particulate matter impact on COVID-19 in Milan, Italy](https://www.sciencedirect.com/science/article/pii/S0048969720333453)|Contact Professor (Shared data)|Done|1025608|1028021|1026036 
**PR8**|Role of chronic air pollution levels in COVID-19|2020|[Role of the chronic air pollution levels in the COVID-19 outbreak risk in Italy](https://www.sciencedirect.com/science/article/pii/S0269749120332115)|Contact Professor (Shared data)|Done|0001036146|0001034281|0001046500
**PR9**|Pollution and COVID-19 pandemic in California|2020|[Correlation between environmental pollution indicators and COVID-19 pandemic: A brief study in Californian context](https://www.sciencedirect.com/science/article/pii/S0013935120305454?via%3Dihub)|Contact Professor (Shared data)|Done|1036547|1034898|1049441
**PR10**|Clustering techniques|2021|[A comparative study of clustering techniques applied on COVID-19 scientific literature](https://ieeexplore.ieee.org/document/9340213)|Contact Professor (Shared data)|Done|1034853|1027021|1900097616
**PR11**|Climate indicators and COVID-19 pandemic in New York|2020|[Correlation between climate indicators and COVID-19 pandemic in New York, USA](https://www.sciencedirect.com/science/article/pii/S0048969720323524#:~:text=Climate%20indicators%20are%20integral%20in,useful%20in%20suppressing%20COVID%2D19.)|Contact Professor (Shared data)|Done|1025474|NA|NA
**PR12**|Particular Matter and COVID-19|2020|[Particulate matter pollution and the COVID-19 outbreak: results from Italian regions and provinces](https://www.archivesofmedicalscience.com/Particulate-matter-pollution-and-the-COVID-19-outbreak-results-from-Italian-regions,122328,0,2.html)|Contact Professor (Shared data)|Done|0001042345|0000973616|NA
**PR13**|Pollutants and COVID-19|2020|[Air pollutants and risks of death due to COVID-19 in Italy](https://www.sciencedirect.com/science/article/pii/S0013935120313566)|Contact Professor (Shared data)|Done|0001043421|0001043418|NA
**PR27**|ML and COVID 19|2020|[Applications of machine learning and artificial intelligence for Covid-19 (SARS-CoV-2) pandemic: A review](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7315944/), [Real-time forecasts and risk assessment of novel coronavirus (COVID-19) cases: A data-driven analysis](https://www.sciencedirect.com/science/article/pii/S0960077920302502)|Contact Professor (Shared data)|Done|0001033845|0001037146|NA
**PR28**|Cluster analysis|2021|[The Use of Cluster Analysis to Evaluate the Impact of COVID-19 Pandemic on Daily Water Demand Patterns](https://www.mdpi.com/2071-1050/13/11/5772/pdf)|Contact Professor (Shared data)|Done|0000935757|NA|NA
**PR29**|ML and COVID 19|2021|[Machine learning-based prediction of COVID-19 diagnosis based on symptoms](https://www.nature.com/articles/s41746-020-00372-6)|Available|Done|0001025121|0001036825|NA
**PR30**|NO2 and COVID19|2022|[Satellite data and machine learning reveal a significant correlation between NO2 and COVID-19 mortality](https://www.sciencedirect.com/science/article/pii/S0013935121012652)|Available|Done|0001044965|0001041166|0001052195
**PR31**|COVID-19 Pandemic,Human Mobility and Air Quality|2021|[Machine Learning on the COVID-19 Pandemic,Human Mobility and Air Quality: A Review](https://arxiv.org/pdf/2104.04059.pdf)|Contact Professor (Shared data)|Done|0001039360|0001044939|NA
**PR32**|Sentiment analysis|2021|[Sentiment Analysis of Covid-19 Tweets using Evolutionary Classification-Based LSTM Model](https://arxiv.org/ftp/arxiv/papers/2106/2106.06910.pdf)|Contact Professor (Shared data)|Done|0001044209|NA|NA
**PR33**|Feature selection|2021|[Efficient Analysis of COVID-19 Clinical Data using Machine Learning Models](https://arxiv.org/pdf/2110.09606.pdf)|Available|Done|0001047112|NA|NA
**PR34**|COVID and mortality|2021|[A machine learning based exploration of COVID-19 mortality risk](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0252384)|Available|Done|1900096852|NA|NA

SOFTWARE METRICS

Identifier|Title|Year|URL|Data|State|BN1|BN2|BN3 
-|-|-|-|-|-|-|-|-
**PR14**|Software Defect Prediction|2021|[Software Defect Prediction for Healthcare Big Data: An Empirical Evaluation of Machine Learning Techniques](https://www.hindawi.com/journals/jhe/2021/8899263/)|Available|Done|0001025642|NA|NA
**PR15**|Software Complexity Prediction|2016|[Software Complexity Prediction by Using Basic Attributes](https://ijaers.com/detail/software-complexity-prediction-by-using-basic-attributes-2/)|Available|Done|0001026186|NA|NA
PR16|Developer Modelling|2017|[Developer Modelling using Software Quality Metrics and Machine Learning](https://www.scitepress.org/Papers/2017/63271/63271.pdf)|Available|Not Available|0001045238|NA|NA
**PR17**|Defect Prediction|2020|[Software Defect Prediction Using Machine Learning](https://ieeexplore.ieee.org/document/9142909)|Available|Done|1030584|NA|NA
**PR18**|Cross-project defect prediction|2016|[Cross-Project Defect Prediction Using a Connectivity-Based Unsupervised Classifier](https://seal-queensu.github.io/publications/pdf/ICSE-Feng-2016.pdf)|Available|Done|1900095389|1900095675|NA
PR19|Predicting Software Defects|2021|[A Survey on Data Science Techniques for Predicting Software Defects](https://link.springer.com/chapter/10.1007/978-3-030-75078-7_31)|Available|Not Available|0001037640|NA|NA
**PR20**|Defect Predictions|2020|[Understanding Machine Learning Software Defect Predictions](https://link.springer.com/article/10.1007/s10515-020-00277-4)|Available|Done|0001032730|NA|NA
**PR21**|Unlabelled datasets|2015|[CLAMI: Defect Prediction on Unlabeled Datasets T](https://ieeexplore.ieee.org/document/7372033)|Available|Done|0000980327|0000974254|NA
**PR22**|Complexity Prediction|2020|[Performance Analysis of Machine Learning Approaches in Software Complexity Prediction](https://link.springer.com/chapter/10.1007%2F978-981-33-4673-4_3)|Available|Done|0001036426|NA|NA
**PR23**|Software Maintainability Predictions|2019|[Deep Learning Approach for Software Maintainability Metrics Prediction](https://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=8698760)|Available|Done|0001031975|NA|NA
PR24|Complex Networks|2020|[Structure, Dynamics, and Applications of Complex Networks in Software Engineering](https://www.hindawi.com/journals/mpe/2020/6038619/)|Available|Not Available|0001038388|NA|NA
PR35|Features selection|2016|[Automatically Learning Semantic Features for Defect Prediction](https://www.cs.purdue.edu/homes/lintan/publications/deeplearn-icse16.pdf)|Avallable|Not Available|0001036426|NA|NA
PR36|Software quality prediction|2020|[Comparison of Machine Learning Techniques for Software Quality Prediction](https://www.sciencegate.app/app/document/download/10.4018/ijkss.2020040102)|Not Available|Available|0001026017|NA|NA
**PR37**|Tests with NLP|2021|[Robustness Tests of NLP Machine Learning Models: Search and Semantically Replace](https://arxiv.org/ftp/arxiv/papers/2104/2104.09978.pdf)|Available|Done|0001039698|0001046813|0001025804
PR38|Feature selection|2008|[Investigating the effect of dataset size, metrics sets, and feature selection techniques on software fault prediction problem](https://www.researchgate.net/publication/222124541_Investigating_the_effect_of_dataset_size_metrics_sets_and_feature_selection_techniques_on_software_fault_prediction_problem)|Available|Available|
**PR39**|Commit Classification|2020|[Commit Classification using Natural Language Processing: Experiments over Labeled Datasets](https://cibse2020.ppgia.pucpr.br/images/artigos/4/S04_P1.pdf)|Available|Done|1051431|NA|NA
PR40|Feature selection|2022|[Evaluating the impact of feature selection consistency in software prediction](https://pdf.sciencedirectassets.com/271600/1-s2.0-S0167642321X0010X/1-s2.0-S0167642321001088/main.pdf?X-Amz-Security-Token=IQoJb3JpZ2luX2VjEIb%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaCXVzLWVhc3QtMSJIMEYCIQCWB0vcNfkwdi7K%2B9%2FMNcD3MAJ7%2BKlAeVdfIIOM4fK8ywIhAJrAFhQwfWN%2FoRHhmeKHsqp5sWJwr740fLQ%2F1fmDytRyKvoDCF8QBBoMMDU5MDAzNTQ2ODY1IgwdiG43rklXl%2F1X%2Bxoq1wM%2Fk0xsUJiCuPINLcxEfjQIoTdajMq4o0oTsEDmU5sSX3UcFzunt3JOAbjV%2F6xDUBACbLaAzAivx%2BFeWQ2MuXVmWLKIh9AShBDcHIdE3%2B3iL3ZVNdNQctpEg3Yqe8ltGs5%2F2bZhvxl0vG%2Bt65SssD1t%2FUyqogdm0bKzH%2Fd4kQVXH2U7jMLbSfbI3AHj8gstU3dOojRx8KsmY0TFeAQRAspP5SFdH2LOEL%2B4xE%2FxnJF4q9us%2FnndKhfwItonj8QzK9f2olKfRr60F%2FLxIm%2B4gzPfwvP6%2BoDsuTGvjfoI%2FhjQIqByS2rvzSL1tQftRzQSQ9eoBQLld8LL9lvscbt%2FVJVNzUUimWxo9MlZN3NxgQPT1FKuR7%2FjAJTL1eFRJVINIolHrdnnVIUvdT6n3hCmnVeALGp9UvpmLoI%2FNx4977MQGojQIOThOSKDvckWADYJJ0Uq8%2FsBeESWUiQfTqBbf9MC6niJu8kVf3MODYv64qs2%2FK907NDs9f7lS39QgQ85GM8GsIAx9HEKpQG4%2BCC%2FW0QY259D24KmmCAP7BrJMAir9%2FEuqJwVnsYdg1GkePrWWQBqiyL8DYa%2FIUL0lqQHxqTB4Db4%2BKOqA0P5wvtg4nuAZi5CgqgdPmowgdmtjQY6pAF1toqqs3qIpBKhLihbLGIGIw1zf14b0qij3htRpzGx6YXJQyNE81D3pWhSGW6VXuqtVN0O%2BNKMGAqbLF0djF7GQmBbfIsIkz7toQHeGT4E1Jc9zsUwrA8b4Yd3JuTltF8LhGOW5QyJ3VrDK1njJaErytb8Jd6g0Fe5%2FN680IjalK1uGIYmN89ifWSM8kX7DJDLAApPKNqsXJBlkTOPzXhMHmtxBQ%3D%3D&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20211204T141851Z&X-Amz-SignedHeaders=host&X-Amz-Expires=300&X-Amz-Credential=ASIAQ3PHCVTY2NZCIYMF%2F20211204%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Signature=2a1e9d6d0bc81dda121fff893fdf2309831017ff4a6bab1d40747bd07fadc373&hash=7ae5b6fac9d6380f4e9f1b198e2d4a54e7ddd17a378b805c724a10747047f5d4&host=68042c943591013ac2b2430a89b270f6af2c76d8dfd086a07176afe7c76c2c61&pii=S0167642321001088&tid=spdf-52eed74b-062a-4214-9aff-13955d890a42&sid=ef663b7f362dc744151af421a3630eddaab6gxrqb&type=client)|Available|Available|||
PR41|Feature Selection|2016|[Feature selection in software defect prediction: A comparative study](https://www.researchgate.net/publication/305675320_Feature_selection_in_software_defect_prediction_A_comparative_study)|Available|Not Available|0001038412|NA|NA
PR42|Feature Selection|2019|[Performance Analysis of Feature Selection Methods in Software Defect Prediction: A Search Method Approach](https://www.mdpi.com/2076-3417/9/13/2764)|Available|Available|||
PR43|Feature Selection|2020|[Impact of Feature Selection Methods on the Predictive Performance of Software Defect Prediction Models: An Extensive Empirical Study](https://www.mdpi.com/2073-8994/12/7/1147)|Available|Available|||
PR44|Fix Commits|2021|[Automated Mapping of Vulnerability Advisories onto their Fix Commits in Open Source Repositories](https://arxiv.org/pdf/2103.13375.pdf)|Contact Professor|Available|||

LOG ANALYSIS

Identifier|Title|Year|URL|Data|State|BN1|BN2|BN3 
-|-|-|-|-|-|-|-|-
**PR25**|Automatic log analysis|2019|[Automatic log analysis with NLP for the CMS workflow handling](https://www.epj-conferences.org/articles/epjconf/abs/2020/21/epjconf_chep2020_03006/epjconf_chep2020_03006.html)|Contact Professor (Shared data)|Done|0001034719|NA|NA
PR26|Log Analysis with NLP|2017|[Experience Report: Log Mining using Natural Language processing and Application to anomaly detection](https://hal.laas.fr/hal-01576291/file/PID4955309.pdf)|Contact Professor (Shared data)|Not Available|0000980205|NA|NA
PR45|Log analysis with CNN|2018|[Detecting Anomaly in Big Data System Logs Using Convolutional Neural Network](http://www.cs.ucf.edu/~lwang/papers/LogCNN2018.pdf)|Contact Professor|Available|||
PR46|NLP and DL|2018|[Anomaly Detection of System Logs Based on Natural Language Processing and Deep Learning](https://www.researchgate.net/publication/329315013_Anomaly_Detection_of_System_Logs_Based_on_Natural_Language_Processing_and_Deep_Learning)|Contact Professor|Available|||
